import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Jeu } from 'src/app/models/jeu';
import { FestivalsService } from 'src/app/services/festivals.service';

@Component({
  selector: 'app-jeu-detail',
  templateUrl: './jeu-detail.component.html',
  styleUrls: ['./jeu-detail.component.css']
})
export class JeuDetailComponent implements OnChanges {
  @Input() jeuToEdit!: Jeu;
  @Output() editedJeu: EventEmitter<Jeu> = new EventEmitter();
  @Output() deletedJeu: EventEmitter<Jeu> = new EventEmitter();
  creatingNewGame: boolean = false;
  jeuGroup!: FormGroup;

  constructor(private fb: FormBuilder, private festivalsService: FestivalsService) {}

  onSubmit() {
    this.jeuToEdit.nom = this.jeuGroup?.get("name")?.value;
    this.jeuToEdit.type = this.jeuGroup?.get("type")?.value;
    this.jeuToEdit.ageMin = this.jeuGroup?.get("ageMin")?.value;
    this.jeuToEdit.ageMax = this.jeuGroup?.get("ageMax")?.value;
    this.jeuToEdit.nbJoueursMin = this.jeuGroup?.get("nbMin")?.value;
    this.jeuToEdit.nbJoueursMax = this.jeuGroup?.get("nbMax")?.value;
    this.jeuToEdit.dureeMinutes = this.jeuGroup?.get("duree")?.value;

    this.editedJeu.emit(this.jeuToEdit);
  }

  deleteJeu() {
    this.deletedJeu.emit(this.jeuToEdit);
    this.creatingNewGame = true;
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.jeuToEdit);
    if (!this.jeuToEdit.id) {
      this.creatingNewGame = true;
    }
    else {
      this.creatingNewGame = false;
    }
    this.jeuGroup = this.fb.group({
      name: [this.jeuToEdit.nom, Validators.required],
      type: [this.jeuToEdit.type],
      ageMin: [this.jeuToEdit.ageMin],
      ageMax: [this.jeuToEdit.ageMax],
      nbMin: [this.jeuToEdit.nbJoueursMin],
      nbMax: [this.jeuToEdit.nbJoueursMax],
      duree: [this.jeuToEdit.dureeMinutes]
    });
  }
}
