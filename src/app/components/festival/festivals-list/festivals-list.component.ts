import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Festival } from 'src/app/models/festival';
import { FestivaljsonService } from 'src/app/services/festivaljson.service';

@Component({
  selector: 'app-festivals-list',
  templateUrl: './festivals-list.component.html',
  styleUrls: ['./festivals-list.component.css']
})
export class FestivalsListComponent {
  @Input() festivals!: Festival[] | null;

  constructor(private router: Router) {}

  editFestival(festival: Festival) {
    this.router.navigate(['festival/' + festival?.id ]);
  }

  createFestival() {
    this.router.navigate(['festival/create']);
  }
}
