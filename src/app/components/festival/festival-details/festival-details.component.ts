import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Editeur } from 'src/app/models/editeur';
import { Festival } from 'src/app/models/festival';
import { FestivalsService } from 'src/app/services/festivals.service';

@Component({
  selector: 'app-festival-details',
  templateUrl: './festival-details.component.html',
  styleUrls: ['./festival-details.component.css']
})
export class FestivalDetailsComponent implements OnInit, OnChanges {
  @Input() festival!: Festival;
  creatingNewFestival: boolean = false;

  festivalGroup!: FormGroup;

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private router: Router, private festivalsService: FestivalsService) {}

  ngOnInit() {
    if (this.route.snapshot.paramMap.has('festivalId')) {
      const id = this.route.snapshot.paramMap.get('festivalId');
      if (id) {
        this.festivalsService.getFestival(id).subscribe({
          next:(fest) => {
            console.log("next")
            console.log(fest)
            this.festival = fest;
            this.festivalsService.getEditors()
            .subscribe(editors => {
              this.festival.editeursObj = editors;

              for (let i = editors.length-1; i >= 0; i--) {
                if(!editors[i].nomSociete) {
                  this.deleteEditorRef(editors[i]);
                }
              }
            });
            this.updateFormFromFestival();
          },
          error:(msg) => {
            this.festival = new Festival("");
            this.updateFormFromFestival();
            this.creatingNewFestival = true;
          }
        });
      }
    }
  }

  updateFormFromFestival() {
    this.festivalGroup = this.fb.group({
      name: [this.festival.name, Validators.required],
      entranceTables: [this.festival.tablebooked_1],
      mainTables: [this.festival.tablebooked_2],
      buffetTables: [this.festival.tablebooked_3]
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.updateFormFromFestival();
  }

  onSubmit() {
    this.festival.name = this.festivalGroup?.get("name")?.value;
    this.festival.tablebooked_1 = this.festivalGroup?.get("entranceTables")?.value | 0;
    this.festival.tablebooked_2 = this.festivalGroup?.get("mainTables")?.value | 0;
    this.festival.tablebooked_3 = this.festivalGroup?.get("buffetTables")?.value | 0;
    if (this.creatingNewFestival) {
      this.festivalsService.addNewFestival(this.festival);
    }
    else {
      this.festivalsService.addUpdateFestival(this.festival);
    }
    this.router.navigate(['']);
  }

  deleteFestival() {
    this.festivalsService.deleteFestival(this.festival);
    this.creatingNewFestival = true;
    this.router.navigate(['']);
  }

  deleteEditorRef(editor: Editeur) {
    this.festivalsService.deleteEditorRef(editor, this.festival);
  }

  editEditor(editor: Editeur) {
    this.router.navigate(['editeur/' + editor?.id ]);
  }

  addNewEditor() {
    this.router.navigate(['festival/' + this.festival.id + '/add-editor']);
  }
}
