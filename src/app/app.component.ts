import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Festival } from './models/festival';
import { FestivaljsonService } from './services/festivaljson.service';
import { FestivalsService } from './services/festivals.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'FestivalApp';
  festivals!: Observable<Festival[]>;

  constructor(private festivalsService: FestivalsService) {}

  ngOnInit() {
    this.festivals = this.festivalsService.getAllFestivals();
  }
}
