import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { EditeurComponent } from './components/festival/editeur/editeur.component';
import { FestivalDetailsComponent } from './components/festival/festival-details/festival-details.component';
import { FestivalsListComponent } from './components/festival/festivals-list/festivals-list.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

const routes: Routes = [
  {path: 'festivals', component: FestivalsListComponent},
  {path: 'festival/:festivalId', component: FestivalDetailsComponent},
  {path: 'editeur/:id', component: EditeurComponent},
  {path: 'festival/:festivalId/add-editor', component: EditeurComponent},
  {path: 'App', component: AppComponent},
  {path: '', redirectTo: '/App', pathMatch: 'full'},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
