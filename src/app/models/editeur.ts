import { Jeu } from "./jeu";
import { Optional } from "@angular/core";

export class Editeur {
    constructor(
        public nomSociete: string,
        @Optional() public id?: string,
        @Optional() public contacts?: string,
        @Optional() public jeux?: Jeu[]
    ) {}
}