export class Jeu {
    constructor(
        public nom: string,
        public id?: string,
        public type: string = "enfant",
        public ageMin: number = 3,
        public ageMax: number = 100,
        public nbJoueursMin: number = 2,
        public nbJoueursMax: number = 100,
        public dureeMinutes: number = 5
        ) {}
}